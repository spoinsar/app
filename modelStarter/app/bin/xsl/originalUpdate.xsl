<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * LICENCE[[
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1/CeCILL 2.O 
 *
 * The contents of this file are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/ 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the 
 * License. 
 * 
 * The Original Code is kelis.fr code. 
 * 
 * The Initial Developer of the Original Code is 
 * antoine.pourchez@kelis.fr 
 * 
 * Portions created by the Initial Developer are Copyright (C) 2005 
 * the Initial Developer. All Rights Reserved. 
 * 
 * Contributor(s): 
 * 
 * 
 * Alternatively, the contents of this file may be used under the terms of 
 * either of the GNU General Public License Version 2 or later (the "GPL"), 
 * or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"), 
 * or the CeCILL Licence Version 2.0 (http://www.cecill.info/licences.en.html), 
 * in which case the provisions of the GPL, the LGPL or the CeCILL are applicable 
 * instead of those above. If you wish to allow use of your version of this file 
 * only under the terms of either the GPL, the LGPL or the CeCILL, and not to allow 
 * others to use your version of this file under the terms of the MPL, indicate 
 * your decision by deleting the provisions above and replace them with the notice 
 * and other provisions required by the GPL, the LGPL or the CeCILL. If you do not 
 * delete the provisions above, a recipient may use your version of this file under 
 * the terms of any one of the MPL, the GPL, the LGPL or the CeCILL.
 * ]]LICENCE
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svn="jware.svn4ant.client">
	<xsl:output encoding="ISO-8859-1" method="xml"/>
	<xsl:param name="pLogRep"/>
	<xsl:param name="pSilent"/>
	
	<xsl:variable name="app.name">wspDeriver : createWsp</xsl:variable>
	
	<xsl:include href="xUtil.xsl"/>
	<xsl:include href="xCommonTarget.xsl"/>
	  
	<xsl:template match="/wspTransformer">
		<project name="doAutoDiff" default="Main" basedir=".."><!-- Basedir d�fini dans le build principal : bin -->
			<taskdef classname="com.scenari.ant.SplashSpecialTask" name="splashspecial"/>
			<taskdef classname="com.scenari.ant.XmlDoctype" name="xmldoctype"/>
			<taskdef resource="net/sf/antcontrib/antcontrib.properties"/>
			<taskdef uri="jware.svn4ant.client" resource="org/jwaresoftware/svn4ant/client/antlib-ns.xml"/>
			
			<property name="app.out.path" location="${{app.conf.path}}/{wspOut/@dir}"/>
			<property name="app.in.path" location="${{app.conf.path}}"/>
			<tstamp><format property="timestamp" pattern="yyMMdd-HHmm"/></tstamp>
			<property name="app.fileTrace.path" location="{$pLogRep}/originalUpdate_${{timestamp}}.log"/>
			
			<target name="Main">
				<record action="start" name="${{app.fileTrace.path}}" loglevel="info"/>
				<trycatch property="x.log.analyse.error">
					<try>
						<xsl:call-template name="xSetSvnProps"/>
						<xsl:apply-templates select="originalFolders"/>
						<echo>---- Les diff�rents updates ont �t� r�alis�s avec succ�s</echo>
					</try>
					<catch>
						<echo>ERREUR : ${x.log.analyse.error}</echo>
					</catch>
				</trycatch>
				<record action="stop" name="${{app.fileTrace.path}}"/>
			</target>
			
		</project>
	</xsl:template>
	
	<xsl:template match="svnSynchro">
		<!-- Outil SVN : [svn4ant,commandLine] -->
		<xsl:variable name="vSvnMode">
			<xsl:call-template name="tSvnMode"/>
		</xsl:variable>
		<xsl:variable name="vSvnCredential4commandLine">
			<xsl:call-template name="tSvnCredentialArgs4commandLine"/>
		</xsl:variable>
		
		<xsl:for-each select="item">
			<var name="itemPath" value="${{app.in.path}}/{@to}"/>
			<if>
				<available file="${{itemPath}}"/>
				<then>
					<!-- le r�pertoire existe d�ja => update -->
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">info</xsl:with-param>
						<xsl:with-param name="pMessage">Update du r�pertoire '${app.in.path}/<xsl:value-of select="@to"/>'.</xsl:with-param>
					</xsl:call-template>
					
					<xsl:choose>
						<xsl:when test="$vSvnMode='svn4ant'">
							<!-- SVN : svn4ant -->
							<svn:update path="${{itemPath}}" feedback="verbose"><!-- quiet -->
								<xsl:if test="../credential">
									<xsl:attribute name="credential"><xsl:value-of select="generate-id(..)"/></xsl:attribute>
								</xsl:if>
								<xsl:if test="@revision">
									<xsl:attribute name="revision"><xsl:value-of select="@revision"/></xsl:attribute>
								</xsl:if>
							</svn:update>
						</xsl:when>
						<xsl:otherwise>
							<!-- SVN : commandline -->
							<exec executable="${{vSvnPath4commandLine}}" dir="${{app.in.path}}" failonerror="true">
								<arg value="update"/>
								<xsl:copy-of select="$vSvnCredential4commandLine"/>
								<arg value="${{itemPath}}"/>
							</exec>
						</xsl:otherwise>
					</xsl:choose>
				</then>
				<else>
					<!-- le r�pertoire n'existe pas => checkOut -->
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">info</xsl:with-param>
						<xsl:with-param name="pMessage">CheckOut du r�pertoire '${app.in.path}/<xsl:value-of select="@to"/>'.</xsl:with-param>
					</xsl:call-template>
					
					<xsl:choose>
						<xsl:when test="$vSvnMode='svn4ant'">
							<!-- SVN : svn4ant -->
							<svn:checkout to="${{app.in.path}}" feedback="verbose">
								<xsl:if test="../credential">
									<xsl:attribute name="credential"><xsl:value-of select="generate-id(..)"/></xsl:attribute>
								</xsl:if>
								<item from="{@from}" to="{@to}">
									<xsl:if test="@revision">
										<xsl:attribute name="revision"><xsl:value-of select="@revision"/></xsl:attribute>
									</xsl:if>
								</item>
							</svn:checkout>
						</xsl:when>
						<xsl:otherwise>
							<!-- SVN : commandline -->
							<mkdir dir="${{app.in.path}}"/>
							<exec executable="${{vSvnPath4commandLine}}" dir="${{app.in.path}}" failonerror="true">
								<arg value="checkout"/>
								<xsl:if test="@revision">
									<arg value="-r {@revision}"/>
								</xsl:if>
								<xsl:copy-of select="$vSvnCredential4commandLine"/>
								<arg value="{@from}"/>
								<arg value="{@to}"/>
							</exec>
						</xsl:otherwise>
					</xsl:choose>
				</else>
			</if>
			<!-- svn ignore de cet �l�ment dans le rep parent -->
			<!-- FIXME A tester
			<svn:propset path="${{app.in.path}}" append="true">
				<xsl:if test="../credential">
					<xsl:attribute name="credential"><xsl:value-of select="generate-id(..)"/></xsl:attribute>
				</xsl:if>
				<property name="svn:ignore" value=""/>
			</svn:propset>
			
			<svn action="propset">
				<argument value="svn:ignore binaries ${{app.in.path}}/../orininal"/>
			</svn>
			 -->
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
