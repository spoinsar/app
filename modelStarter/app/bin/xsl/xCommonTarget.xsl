<?xml version="1.0" encoding="UTF-8"?>
<!--
 * LICENCE[[
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1/CeCILL 2.O 
 *
 * The contents of this file are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/ 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the 
 * License. 
 * 
 * The Original Code is kelis.fr code. 
 * 
 * The Initial Developer of the Original Code is 
 * antoine.pourchez@kelis.fr 
 * 
 * Portions created by the Initial Developer are Copyright (C) 20058
 * the Initial Developer. All Rights Reserved. 
 * 
 * Contributor(s): 
 * 
 * 
 * Alternatively, the contents of this file may be used under the terms of 
 * either of the GNU General Public License Version 2 or later (the "GPL"), 
 * or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"), 
 * or the CeCILL Licence Version 2.0 (http://www.cecill.info/licences.en.html), 
 * in which case the provisions of the GPL, the LGPL or the CeCILL are applicable 
 * instead of those above. If you wish to allow use of your version of this file 
 * only under the terms of either the GPL, the LGPL or the CeCILL, and not to allow 
 * others to use your version of this file under the terms of the MPL, indicate 
 * your decision by deleting the provisions above and replace them with the notice 
 * and other provisions required by the GPL, the LGPL or the CeCILL. If you do not 
 * delete the provisions above, a recipient may use your version of this file under 
 * the terms of any one of the MPL, the GPL, the LGPL or the CeCILL.
 * ]]LICENCE
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svn="jware.svn4ant.client">
	<xsl:output encoding="UTF-8" method="xml"/>
	
	<!-- # Gestion SVN -->
	<!-- @return [commandLine,svn4ant] -->
	<xsl:template name="tSvnMode">
		<xsl:choose>
			<xsl:when test="/wspTransformer/originalFolders/svnSynchro/@useCommandLineClient='true'">commandLine</xsl:when>
			<xsl:otherwise>svn4ant</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="xSetSvnProps">
		<xsl:variable name="vSvnMode">
			<xsl:call-template name="tSvnMode"/>
		</xsl:variable>
		<xsl:if test="$vSvnMode='svn4ant'">
			<echo>SVN-svn4ant</echo>
		</xsl:if>
		<xsl:if test="$vSvnMode='commandLine'">
			<!-- Recherche de l'executable svn :
					1- dans le PATH de l'OS
					2- via SUBVERSION_HOME
			 -->
			<condition property="x.svn.exe" value="svn.exe" else="svn">
				<os family="windows"/>
			</condition>
			<if>
				<available file="${{x.svn.exe}}"/>
				<then>
					<echo>SVN-commandLine : depuis le PATH</echo>
					<property name="vSvnPath4commandLine" value="${{x.svn.exe}}"/>
				</then>
				<else>
					<property environment="env"/>
					<echo>SVN-commandLine : depuis la var SUBVERSION_HOME</echo>
					<property name="vSvnPath4commandLine" value="${{env.SUBVERSION_HOME}}/bin/${{x.svn.exe}}"/>
				</else>
			</if>
			
			<!-- Contrôles -->
			<fail message="Executable SVN introuvable. Veuillez définir la variable d'environnement SUBVERSION_HOME, et vérifier que l'executable '${{vSvnPath4commandLine}}' est présent.">
				<condition>
					<not><available file="${{vSvnPath4commandLine}}"/></not>
				</condition>
			</fail>
		</xsl:if>
		<xsl:if test="/wspTransformer/originalFolders/svnSynchro/credential and $vSvnMode='svn4ant'">
			<svn:credential id="{generate-id()}">
				<xsl:copy-of select="credential/@*"/>
			</svn:credential>
		</xsl:if>
	</xsl:template>
	<xsl:template name="tSvnCredentialArgs4commandLine">
		<arg value="--username"/>
		<arg value="{credential/@username}"/>
		<arg value="--password"/>
		<arg value="{credential/@password}"/>
	</xsl:template>
	
	<xsl:template name="xCommonTarget">
		<!-- Supprime un fichier (SVN )-->
		<target name="xDeleteSvnFile">
			<echo>xDeleteSvnFile - suppression du fichier : ${pFileToDelete}</echo>
			<xsl:variable name="vSvnMode">
				<xsl:call-template name="tSvnMode"/>
			</xsl:variable>
			<trycatch property="foo" reference="bar">
				<try>
					<xsl:choose>
						<xsl:when test="$vSvnMode='svn4ant'">
							<svn:delete path="${{pFileToDelete}}"/>
						</xsl:when>
						<xsl:otherwise>
							<exec executable="${{vSvnPath4commandLine}}" dir="${{app.in.path}}" failonerror="true">
								<arg value="delete"/>
								<arg value="${{pFileToDelete}}"/>
							</exec>
						</xsl:otherwise>
					</xsl:choose>
				</try>
				<catch>
					<delete file="${{pFileToDelete}}"/>
				</catch>
			</trycatch>
		</target>
		<!-- Supprime un répertoire (SVN )-->
		<target name="xDeleteSvnDir">
			<echo>xDeleteSvnDir - suppression du répertoire : ${pDirToDelete}</echo>
			<xsl:variable name="vSvnMode">
				<xsl:call-template name="tSvnMode"/>
			</xsl:variable>
			<trycatch property="foo" reference="bar">
				<try>
					<xsl:choose>
						<xsl:when test="$vSvnMode='svn4ant'">
							<svn:delete path="${{pFileToDelete}}"/>
						</xsl:when>
						<xsl:otherwise>
							<exec executable="${{vSvnPath4commandLine}}" dir="${{app.in.path}}" failonerror="true">
								<arg value="delete"/>
								<arg value="${{pFileToDelete}}"/>
							</exec>
						</xsl:otherwise>
					</xsl:choose>
				</try>
				<catch>
					<delete dir="${{pDirToDelete}}"/>
				</catch>
			</trycatch>
		</target>
	</xsl:template>
 
</xsl:stylesheet>
