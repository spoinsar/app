<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * LICENCE[[
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1/CeCILL 2.O 
 *
 * The contents of this file are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/ 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the 
 * License. 
 * 
 * The Original Code is kelis.fr code. 
 * 
 * The Initial Developer of the Original Code is 
 * antoine.pourchez@kelis.fr 
 * 
 * Portions created by the Initial Developer are Copyright (C) 2005 
 * the Initial Developer. All Rights Reserved. 
 * 
 * Contributor(s): 
 * 
 * 
 * Alternatively, the contents of this file may be used under the terms of 
 * either of the GNU General Public License Version 2 or later (the "GPL"), 
 * or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"), 
 * or the CeCILL Licence Version 2.0 (http://www.cecill.info/licences.en.html), 
 * in which case the provisions of the GPL, the LGPL or the CeCILL are applicable 
 * instead of those above. If you wish to allow use of your version of this file 
 * only under the terms of either the GPL, the LGPL or the CeCILL, and not to allow 
 * others to use your version of this file under the terms of the MPL, indicate 
 * your decision by deleting the provisions above and replace them with the notice 
 * and other provisions required by the GPL, the LGPL or the CeCILL. If you do not 
 * delete the provisions above, a recipient may use your version of this file under 
 * the terms of any one of the MPL, the GPL, the LGPL or the CeCILL.
 * ]]LICENCE
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="ISO-8859-1" method="xml"/>
	<xsl:param name="pLogRep"/>
	<xsl:param name="pSilent"/>
	
	<xsl:variable name="app.subname">autoDiff</xsl:variable>
	
	
	
	<xsl:include href="xUtil.xsl"/>
	<xsl:include href="xCommonTarget.xsl"/>
	  
	<xsl:template match="/wspTransformer">
		<project name="doAutoDiff" default="Main" basedir=".."><!-- Basedir d�fini dans le build principal : bin -->
			<taskdef classname="com.scenari.ant.SplashSpecialTask" name="splashspecial"/>
			<taskdef classname="com.scenari.ant.XmlDoctype" name="xmldoctype"/>
			<taskdef resource="net/sf/antcontrib/antcontrib.properties"/>
			<taskdef uri="jware.svn4ant.client" resource="org/jwaresoftware/svn4ant/client/antlib-ns.xml"/>
			
			<property name="app.subname" value="{$app.subname}"/>
			<property name="app.out.path" location="${{app.conf.path}}/{wspOut/@dir}"/>
			<property name="app.in.path" location="${{app.conf.path}}"/>
			<tstamp><format property="timestamp" pattern="yyMMdd-HHmm"/></tstamp>
			<property name="app.fileTrace.path" location="{$pLogRep}/${{app.subname}}_${{timestamp}}.log"/>
			
			<target name="Main">
				<record action="start" name="${{app.fileTrace.path}}" loglevel="info"/>
				<trycatch property="x.log.analyse.error">
					<try>
						<xsl:call-template name="xSetSvnProps"/>
						<xsl:apply-templates select="overwrite"/>
						<echo>---- Le diff de l'atelier a �t� r�alis� avec succ�s</echo>
					</try>
					<catch>
						<echo>ERREUR : ${x.log.analyse.error}</echo>
					</catch>
				</trycatch>
				<record action="stop" name="${{app.fileTrace.path}}"/>
			</target>
			
			<xsl:apply-templates mode="target"/>
			<xsl:call-template name="xCommonTarget"/>
		</project>
	</xsl:template>

	
	<!-- # overwrite -->
	<xsl:template match="overwrite">
		<xsl:comment># overwrite de '<xsl:value-of select="@from"/>' vers '<xsl:value-of select="@to"/>'</xsl:comment>
		<echo># overwrite : de '<xsl:value-of select="@from"/>' vers '<xsl:value-of select="@to"/>'</echo>
		<property name="vOverwritePath" location="${{app.conf.path}}/{@from}"/>
		<mkdir dir="${{vOverwritePath}}"/>
		<xsl:if test="$pSilent='no'">
			<splashspecial imageurl="${{app.res.path}}/logo.png" texte2="{/wspTransformer/@name}" texte="Diff�rentiel de '{@to}'..."/>
		</xsl:if>
		<patternset id="fs.exclude.global">
		      <xsl:for-each select="//xsl[@includes or @excludes]">
		          <patternset>
		              <xsl:if test="@includes"><xsl:attribute name="excludes"><xsl:value-of select="@includes"/></xsl:attribute></xsl:if>
		              <xsl:if test="@excludes"><xsl:attribute name="includes"><xsl:value-of select="@excludes"/></xsl:attribute></xsl:if>
		          </patternset>
		      </xsl:for-each>
		</patternset>
		<!-- # gestion des r�pertoires de d�rivation -->
		<xsl:for-each select="/*/originalFolders/folder">
			<ant target="tDiffOvr_{generate-id()}" antfile="${{app.tmp.build.path}}" inheritrefs="true"/>
		</xsl:for-each>
		<!-- # r�cup�ration des autres r�pertoires -->
		<copy todir="${{vOverwritePath}}">
			<fileset dir="${{app.out.path}}">
				<xsl:if test="string-length(normalize-space(@excludes))&gt;0"><xsl:attribute name="excludes"><xsl:value-of select="@excludes"/></xsl:attribute></xsl:if>
				<xsl:if test="string-length(normalize-space(@includes))&gt;0"><xsl:attribute name="includes"><xsl:value-of select="@includes"/></xsl:attribute></xsl:if>
				<xsl:for-each select="/*/originalFolders/folder">
					<exclude name="{@target}/**"/>
				</xsl:for-each>
			</fileset>
		</copy>
		<!-- # cleanUp des autres r�pertoires que les reps de d�rivation -->
		<fileset id="fs.file.deleted" dir="${{vOverwritePath}}" includes="**/**">
			<xsl:for-each select="/*/originalFolders/folder">
				<exclude name="{@target}/**"/>
			</xsl:for-each>
			<not><present targetdir="${{app.out.path}}"/></not>
		</fileset>
		<dirset id="fs.dir.deleted" dir="${{vOverwritePath}}" includes="**/**">
			<xsl:for-each select="/*/originalFolders/folder">
				<exclude name="{@target}/**"/>
			</xsl:for-each>
			<not><present targetdir="${{app.out.path}}"/></not>
		</dirset>
		 <zip destfile="{$pLogRep}/${{app.subname}}_${{timestamp}}.bkp.zip" update="true"><!-- backup -->
		 	<fileset refid="fs.file.deleted"/>
  		      	<dirset refid="fs.dir.deleted"/>
		 </zip>
		<foreach target="xDeleteSvnFile" param="pFileToDelete"><!-- supp des fichiers -->
			<path>
				<fileset refid="fs.file.deleted"/>
			</path>
		</foreach>
		<foreach target="xDeleteSvnDir" param="pDirToDelete"><!-- supp des reps -->
			<path>
				<dirset refid="fs.dir.deleted"/>
			</path>
		</foreach>
	</xsl:template>
	
	<xsl:template match="overwrite" mode="target">
		<xsl:for-each select="/*/originalFolders/folder">
		
			<target name="tDiffOvr_{generate-id()}">
				<property name="vCurrentOverwritePath" location="${{vOverwritePath}}/{@target}"/>
				<!-- # Recherche des fichiers sur lesquels on applique une transformation dans 'overwrite' -->
				<antfetch target="xGetOverwriteExcludeFile" antfile="${{app.tmp.build.path}}" return="fs.exclude.overwrite"/>
				
				<!-- # R�cup�ration des fichiers modifi�s -->
				<fileset id="site.files.modify" dir="${{app.out.path}}/{@target}">
					<xsl:attribute name="includes"><xsl:choose><xsl:when test="string-length(normalize-space(@includes))&gt;0"><xsl:value-of select="@includes"/></xsl:when><xsl:otherwise>**/**</xsl:otherwise></xsl:choose></xsl:attribute>
					<xsl:if test="string-length(normalize-space(@excludes))&gt;0"><xsl:attribute name="excludes"><xsl:value-of select="@excludes"/></xsl:attribute></xsl:if>
					<xsl:if test="string-length(normalize-space(@includes))&gt;0"><xsl:attribute name="includes"><xsl:value-of select="@includes"/></xsl:attribute></xsl:if>
				    <patternset refid="fs.exclude.global"/>
				    <patternset excludes="${{fs.exclude.overwrite}}"/>
				    <different targetdir="${{app.in.path}}/{@from}" ignoreFileTimes="true" ignoreContents="false"/>
				</fileset>
				<pathconvert pathsep="${line.separator}" property="site.files.modify.fileList" refid="site.files.modify"/>
				<if>
					<isset property="site.files.modify.fileList"/>
					<then>
						<echo>- R�cup�ration des fichiers modifi�s : ${site.files.modify.fileList}</echo>
						<xsl:call-template name="tLogMessage">
							<xsl:with-param name="pLevel">info</xsl:with-param>
							<xsl:with-param name="pMessage"># R�cup�ration des fichiers modifi�s suivants : ${site.files.modify.fileList}
							</xsl:with-param>
						</xsl:call-template>
						<copy todir="${{vCurrentOverwritePath}}" overwrite="true" includeemptydirs="false" preservelastmodified="true">
							<fileset refid="site.files.modify"/>
						</copy>
					</then>
				</if>
				
				<!-- # R�cup�ration des fichiers ajout�s -->
				<fileset id="site.files.apend" dir="${{app.in.path}}/{@from}">
					<present present="srconly" targetdir="${{app.out.path}}/{@target}"/>
					<patternset refid="fs.exclude.global"/>
				</fileset>
				<pathconvert pathsep="${line.separator}" property="site.files.apend.fileList" refid="site.files.apend"/>
				<if>
					<isset property="site.files.apend.fileList"/>
					<then>
						<echo>- R�cup�ration des fichiers ajout�s : ${site.files.apend.fileList}</echo>
						<xsl:call-template name="tLogMessage">
							<xsl:with-param name="pLevel">info</xsl:with-param>
							<xsl:with-param name="pMessage"># R�cup�ration des fichiers ajout�s suivants : ${site.files.apend.fileList}
							</xsl:with-param>
						</xsl:call-template>
						<copy todir="${{vCurrentOverwritePath}}" overwrite="true" includeemptydirs="true" preservelastmodified="true">
							<fileset refid="site.files.apend"/>
						</copy>
					</then>
				</if>
			</target>
		</xsl:for-each>
		
		<!-- # Targets utils -->
			
		<!-- Retourne la liste des fichiers sur lesquels est appliqu� une transformation dans 'overwrite' -->
		<target name="xGetOverwriteExcludeFile">
			<if>
				<available file="${{vCurrentOverwritePath}}"/>
				<then>
					<!-- .exec.xsl -->
					<fileset id="virtualFile.xsl" dir="${{vCurrentOverwritePath}}" includes="**/*.{$virtualFile.xsl.extension}"/>
					<pathconvert targetos="unix" pathsep="," property="virtualFile.xsl.fileList" refid="virtualFile.xsl">
						<map from="${{vCurrentOverwritePath}}${{file.separator}}" to=""/>
						<globmapper from="*.{$virtualFile.xsl.extension}" to="*" casesensitive="yes"/>
					</pathconvert>
					<!-- .exec.ant -->
					<fileset id="virtualFile.ant" dir="${{vCurrentOverwritePath}}" includes="**/*.{$virtualFile.ant.extension}"/>
					<pathconvert targetos="unix" pathsep="," property="virtualFile.ant.fileList" refid="virtualFile.ant">
						<map from="${{vCurrentOverwritePath}}${{file.separator}}" to=""/>
						<globmapper from="*.{$virtualFile.ant.extension}" to="*" casesensitive="yes"/>
					</pathconvert>
					<!-- .remove (file) -->
					<fileset id="virtualFile.remove.file" dir="${{vCurrentOverwritePath}}" includes="**/*.{$virtualFile.remove.extension}"/>
					<pathconvert targetos="unix" pathsep="," property="virtualFile.remove.file.fileList" refid="virtualFile.remove.file">
						<map from="${{vCurrentOverwritePath}}${{file.separator}}" to=""/>
						<globmapper from="*.{$virtualFile.remove.extension}" to="*" casesensitive="yes"/>
					</pathconvert>
					<!-- .remove (dir) -->
					<dirset id="virtualFile.remove.dir" dir="${{vCurrentOverwritePath}}" includes="**/*.{$virtualFile.remove.extension}"/>
					<pathconvert targetos="unix" pathsep="," property="virtualFile.remove.dir.fileList" refid="virtualFile.remove.dir">
						<map from="${{vCurrentOverwritePath}}${{file.separator}}" to=""/>
						<globmapper from="*.{$virtualFile.remove.extension}" to="*" casesensitive="yes"/>
					</pathconvert>
					
					<property name="fs.exclude.overwrite" value="${{virtualFile.xsl.fileList}},${{virtualFile.ant.fileList}},${{virtualFile.remove.file.fileList}},${{virtualFile.remove.dir.fileList}}"/>

				</then>
			</if>
		</target>
		
	</xsl:template>
	
	<xsl:template match="*" mode="target"/>
	
</xsl:stylesheet>
