#!/bin/sh
echo "Creation du WSP"
lib="bin/lib/lib"
ant="bin/build.xml"
bsfLibList=$lib/bsf.jar:$lib/ant-apache-bsf.jar:$lib/js.jar:$lib/commons-logging.jar

#Recherche de java et controle que se soit une version SUN
vJavaCmd="java"
xCheckJava () {
	vInputVarName=\$"$1"
	vInputVarVal=`eval "expr \"$vInputVarName\" "`
	if [ -z "$vInputVarVal" ];then
		eval "$1=false"
		return
	fi
	vSunJavaFound=`$vInputVarVal -version 2>&1 | grep -o "HotSpot\|OpenJDK"`
	if [ "$vSunJavaFound" = "" ]; then
		eval "$1=false"
		return
	fi
}
xCheckJava vJavaCmd
if [ "$vJavaCmd" = "false" ]; then
	vJavaCmd="$JAVA_HOME/bin/java"
	xCheckJava vJavaCmd
	if [ "$vJavaCmd" = "false" ]; then
		echo "ERREUR: JRE compatible introuvable. Veuillez déclarer la variable d'environnement JAVA_HOME."
		exit 1
	fi
fi
vProps=""
if echo "$1" | grep -q s ; then
	vProps="$vProps -Dmode.silent=yes"
fi

#Lancer la commande
$vJavaCmd -classpath "$bsfLibList:$lib/xalan/serializer.jar:$lib/xalan/xsltc.jar:$lib/xalan/xalan.jar:$lib/XML/xmltask.jar:$lib/ant-launcher.jar:$lib/scenariant.jar:$lib/ant.jar:$lib/ant-contrib.jar:$lib/ant-nodeps.jar:$lib/ant-trax.jar:" -Xmx150m org.apache.tools.ant.Main -buildfile $ant mCreateWsp $vProps
