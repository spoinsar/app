<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" 
				xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling"
				xmlns:op="utc.fr:ics/opale3" 
				>
	<xsl:output encoding="UTF-8" method="xml"/>

	<!-- 
		# Suppression des appels � latex
	 -->
	<xsl:template match="sm:textLeafTag[@role='mathtex']">
		<xsl:comment>[opaleLight] mathtex supprim�</xsl:comment>
	</xsl:template>
	
	<!-- ### -->
	<!-- # Cas g�n�ral : on copie -->
	<!-- # -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
		
</xsl:stylesheet>
