<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" 
				xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling"
				xmlns:op="utc.fr:ics/opale3" 
				>
	<xsl:output encoding="UTF-8" method="xml"/>

	<!-- 
		# Suppression des filtres
	 -->
	<xsl:template match="sm:meta[@sc:refUri='/academic/model/_edt/filters/filter.model']">
		<xsl:comment>[opaleLight] filtres supprim�es</xsl:comment>
	</xsl:template>
	
	<xsl:template match="sm:subData[@code='filters'][sm:allowedDataModel/@sc:refUri='/academic/model/_edt/filters/filter.model']">
		<xsl:comment>[opaleLight] filtres supprim�es</xsl:comment>
	</xsl:template>
	<!-- filtres en part dans les balises p�dago (res.model) -->
	<xsl:template match="sm:part[@code='filtered']">
		<xsl:comment>[opaleLight] filtres supprim�es</xsl:comment>
	</xsl:template>
	
	
	<!-- ### -->
	<!-- # Cas g�n�ral : on copie -->
	<!-- # -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
		
</xsl:stylesheet>
