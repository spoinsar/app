<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" 
				xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling"
				xmlns:op="utc.fr:ics/opale3" 
				>
	<xsl:output encoding="UTF-8" method="xml"/>

	<!-- 
		# Suppression des appels aux metas info.model dans uM
	 -->
	<xsl:template match="@xpath[.=&quot;/*/*/op:odRootM/sp:settings/sp:solPractUc='yes'&quot;]">
		<xsl:attribute name="xpath">true()</xsl:attribute>
	</xsl:template>
	
	<xsl:template match="@xpath[.=&quot;/*/*/op:odRootM/sp:settings/sp:solQuiz='yes'&quot;]">
		<xsl:attribute name="xpath">true()</xsl:attribute>
	</xsl:template>

	<xsl:template match="@xpath[.=&quot;/*/*/op:odRootM/sp:settings/sp:solAssmntUa='yes'&quot;]">
		<xsl:attribute name="xpath">true()</xsl:attribute>
	</xsl:template>
	
	<!-- ### -->
	<!-- # Cas g�n�ral : on copie -->
	<!-- # -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
		
</xsl:stylesheet>
