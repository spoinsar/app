<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" 
				xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling"
				xmlns:se="http://www.utc.fr/ics/scenari/v3/editing"
				xmlns:op="utc.fr:ics/opale3" 
				>
	<xsl:output encoding="UTF-8" method="xml"/>
	
	<!-- 
		# Suppression du XED, des consignes et index des ressources dans resInfoM.model,instructionM.model et indexM.model
	 -->
	<xsl:template match="sm:field[@code='index']">
		<xsl:comment>[opaleLight] 'Type d'index (papier)' supprim�</xsl:comment>
	</xsl:template>

	<xsl:template match="sm:field[@code='pubMode']">
		<xsl:comment>[opaleLight] 'Mode de publication (web et diaporama)' supprim�</xsl:comment>
	</xsl:template>
	
	<xsl:template match="sm:formEditor">
		<xsl:comment>[opaleLight] 'xed' supprim�</xsl:comment>
	</xsl:template>
	
	<xsl:template match="sm:subData[@code='instruct']">
		<xsl:comment>[opaleLight] 'Consignes' supprim�</xsl:comment>
	</xsl:template>
	
	<xsl:template match="sm:field[@code='webInstruct']">
		<xsl:comment>[opaleLight] 'Consigne (pour le web et diaporama)' supprim�</xsl:comment>
	</xsl:template>
	
	<xsl:template match="sm:field[@code='odInstruct']">
		<xsl:comment>[opaleLight] 'Consigne (pour le papier)' supprim�</xsl:comment>
	</xsl:template>

	
	<!-- ### -->
	<!-- # Cas g�n�ral : on copie -->
	<!-- # -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
		
</xsl:stylesheet>
